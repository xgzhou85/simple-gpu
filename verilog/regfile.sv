module regfile(
    input         clk,
    input         rst_n,
    input         dstreg_write,
    input         src0_read,
    input         src1_read,
    input  [4:0]  dstreg_addr,
    input  [4:0]  src0_addr,
    input  [4:0]  src1_addr,
    input  [31:0] dstreg_data,
    output [31:0] src0_data,
    output [31:0] src1_data
);

reg [31:0] regs[32];
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) for (int i = 0; i < 32; i = i + 1) regs[i] <= 0;
    else if (dstreg_write) regs[dstreg_addr] <= dstreg_data;
    else regs <= regs;
end

assign src0_data = src0_read ? regs[src0_data] : 32'hdeadbeef;
assign src1_data = src1_read ? regs[src1_data] : 32'hdeadbeef;



endmodule
