module valu (
    input  [1023:0] operand0,
    input  [1023:0] operand1,
    output [1023:0] result,

    input [3:0] op,
    input [1:0] sf
);

genvar i;
generate
for (i = 0; i < 32; i = i + 1) begin : VALU_ALUS
    alu u_alu(
        .operand0(operand0[i*32+:32]),
        .operand1(operand1[i*32+:32]),
        .result(result[i*32+:32]),
        .op(op),
        .sf(sf)
    );
end
endgenerate


endmodule
