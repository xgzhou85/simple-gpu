
//=============================================================================
// This module manages reading from and writing to a frame buffer located in a 
// 1GB ddr3 memory interfaced by an avalon-mm interface
//=============================================================================


// localparam PIXELS_HORIZ = 640,
// localparam PIXELS_VERT  = 480,
// localparam FRAMEBUFFER_LOCATION = 'h10000;
// TODO: parameter FRAMEBUFFER_SIZE     = PIXELS_HORIZ * PIXELS_VERT; // need this? 

module framebuffer_intf #(
    parameter PIXELS_HORIZ = 640,
    parameter PIXELS_VERT  = 480
) (
    input clk,
    input rst_n,

    input  wire [$clog2(PIXELS_HORIZ):0] xpos,
    input  wire [$clog2(PIXELS_VERT ):0] ypos,
    output wire [23:0] outdata,
    input  wire read,

    output wire [28:0] framebuffer_address,
    output wire [7:0]  framebuffer_burstcount,
    input  wire        framebuffer_waitrequest,
    input  wire [63:0] framebuffer_readdata,
    input  wire        framebuffer_readdatavalid,
    output wire        framebuffer_read,
    output wire [7:0]  framebuffer_byteenable,

    input  wire [31:0] framebuffer_location,
    input  wire        framebuffer_enabled


);

// // store the pixel we read
// always_ff @(posedge clk, negedge rst_n) begin
//     if (~rst_n) outdata <= 24'hffffff;
//     else if (framebuffer_readdatavalid) outdata <= framebuffer_readdata[23:0];
//     else outdata <= outdata;
// end

assign framebuffer_burstcount = 1;
assign framebuffer_byteneable = 8'b00001111;
assign framebuffer_address = framebuffer_location + (ypos * PIXELS_HORIZ + xpos);
// assign framebuffer_read    = ~framebuffer_waitrequest & read & framebuffer_enabled;
assign framebuffer_read    = read | framebuffer_enabled; // FIXME: this is a mistake
assign outdata = {framebuffer_readdata[23:0]};

endmodule
