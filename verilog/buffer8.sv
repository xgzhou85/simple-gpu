module buffer8 #(
    parameter BUFFER_DEPTH = 256 // this should always be a power of two
)(
    input        clk,
    input        rst_n,
    input  [7:0] din,
    input        insert,
    input        remove,
    output [7:0] dout,
    output       empty,
    output       full
);

    reg [$clog2(BUFFER_DEPTH)-1:0] produce;
    reg [$clog2(BUFFER_DEPTH)-1:0] consume;
    reg [7:0] buffer [0: BUFFER_DEPTH-1];
    
    assign dout = buffer[consume];
    assign empty = consume == produce;
    assign full = consume-1 == produce;

always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) begin 
        produce <= 0;
        consume <= 0;
    end
    else begin
        if (insert) begin
            buffer[produce] <= din;
            produce <= produce + 1;
        end
        if (remove) begin
            buffer[consume] <= 8'h66;
            consume <= consume + 1;
        end
    end
end

endmodule

