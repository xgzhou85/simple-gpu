// TODO: double check fxp issues in alu
// TODO: add overflow etc flags? probably
module alu #(
    parameter  FXP_INT_BITS  = 16
)(
    input      [31:0] operand0,
    input      [31:0] operand1,
    output reg [31:0] result,

    input [3:0] op,
    input [1:0] sf
);
localparam FXP_FRAC_BITS = 32 - FXP_INT_BITS;

wire [31:0] oprnd0;
wire [31:0] oprnd1;

assign oprnd0 = 
    (sf == 2'b00) ? operand0 : // unsigned
    (sf == 2'b01) ? operand0 : // signed
    (sf == 2'b10) ? operand0 << FXP_FRAC_BITS : // signed with fxp
                    operand0 ; // fxp

assign oprnd1 = 
    (sf == 2'b00) ? operand1 : // unsigned
    (sf == 2'b01) ? operand1 : // signed
    (sf == 2'b10) ? operand1 : // signed with fxp
                    operand1 ; // fxp

// FIXME: doing all these ops in parallel is going to make the alu huge, slow, and power hungry
wire [31:0] _add;
wire [31:0] _sub;
wire [31:0] _mul;
wire [31:0] _div;
wire [31:0] _eq;
wire [31:0] _neq;
wire [31:0] _gt;
wire [31:0] _gte;
wire [31:0] _lt;
wire [31:0] _lte;
wire [31:0] _not;
wire [31:0] _or;
wire [31:0] _and;
wire [31:0] _xor;
wire [31:0] _nor;
wire [31:0] _nand;
wire [31:0] _xnor;
wire [31:0] _shfl;
wire [31:0] _shfr;
wire [31:0] _shfa;
wire [31:0] _set;
wire [31:0] _clr;

assign _add  = (sf == 2'b00) ? oprnd0 +  oprnd1 : $signed(oprnd0) +  $signed(oprnd1);
assign _sub  = (sf == 2'b00) ? oprnd0 -  oprnd1 : $signed(oprnd0) -  $signed(oprnd1);
assign _mul  = 0; // (sf == 2'b00) ? oprnd0 *  oprnd1 : $signed(oprnd0) *  $signed(oprnd1);
assign _div  = 0; // (sf == 2'b00) ? oprnd0 /  oprnd1 : $signed(oprnd0) /  $signed(oprnd1);
assign _eq   = (sf == 2'b00) ? oprnd0 == oprnd1 : $signed(oprnd0) == $signed(oprnd1);
assign _neq  = (sf == 2'b00) ? oprnd0 != oprnd1 : $signed(oprnd0) != $signed(oprnd1);
assign _gt   = (sf == 2'b00) ? oprnd0 >  oprnd1 : $signed(oprnd0) >  $signed(oprnd1);
assign _gte  = (sf == 2'b00) ? oprnd0 >= oprnd1 : $signed(oprnd0) >= $signed(oprnd1);
assign _lt   = (sf == 2'b00) ? oprnd0 <  oprnd1 : $signed(oprnd0) <  $signed(oprnd1);
assign _lte  = (sf == 2'b00) ? oprnd0 <= oprnd1 : $signed(oprnd0) <= $signed(oprnd1);
assign _not  = ~oprnd0;
assign _or   = oprnd0 | oprnd1;
assign _and  = oprnd0 & oprnd1;
assign _xor  = oprnd0 ^ oprnd1;
assign _nor  = oprnd0 |~ oprnd1;
assign _nand = oprnd0 &~ oprnd1;
assign _xnor = oprnd0 ~^ oprnd1;
assign _shfl = oprnd0 << oprnd1;
assign _shfr = oprnd0 >> oprnd1;
assign _shfa = oprnd0 >>> oprnd1;
assign _set  = 1;
assign _clr  = 0;

always_comb begin

casez(op)
    // arithmetic
    4'b0000: result = _add;
    4'b0001: result = _sub;
    4'b0010: result = _mul;
    4'b0011: result = _div;

    // comparison
    4'b0100: result = _eq;
    4'b0101: result = _neq;
    4'b0110: result = _gt;
    4'b0111: result = _gte;
    4'b1000: result = _lt;
    4'b1001: result = _lte;

    // logical (numbers or masks)
    4'b1?1?: begin
        case({op[0], sf})
            3'b000: result = _not;
            3'b001: result = _or;
            3'b010: result = _and;
            3'b011: result = _xor;
            3'b100: result = _nor;
            3'b101: result = _nand;
            3'b110: result = _xnor;
            3'b111: result = 32'h00000000;
        endcase
    end

    // shifts
    4'b1101: begin
        case({sf})
            2'b00: result = _shfl;
            2'b01: result = _shfr;
            2'b10: result = _shfa;
            2'b11: result = 32'h00000000;
        endcase

    end

    // the rest
    default: result = 32'h00000000;

endcase
end

endmodule
