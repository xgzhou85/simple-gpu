module decode(
    input [31:0] instruction,

    output unconditional,

    // operands
    output        dstreg_write,
    output        src0_read,
    output        src1_read,
    output [4:0]  dstreg_addr,
    output [4:0]  src0_addr,
    output [4:0]  src1_addr,
    output [22:0] immediate,
    output        use_imm,

    // mask reg control signals
    output dst_mask,
    output src_mask,

    // alu control signals
    output [3:0] op,
    output [1:0] sf,

    // memory control signals
    output load,
    output store,
    output global_mem,
    output shared_mem
);
wire [7:0]  subinstr;
wire [12:0] imm_sm;
wire [22:0] imm_lg;
wire [1:0]  enc;

// memory
assign load       = (op == 4'b1100) && ~subinstr[0];
assign store      = (op == 4'b1100) && subinstr[0];
assign global_mem = (load | store) && ~subinstr[1];
assign shared_mem = (load | store) &&  subinstr[1];

// break apart instruction
// TODO: register reading and writing is slightly more complicated than this
// because of the other instructions that are not recorded in the instruction list yet
assign unconditional = instruction[31];
assign subinstr = instruction[30:23];
assign dstreg_write = (enc == 2'b00) || (enc == 2'b01);
assign src0_read = (enc == 2'b00) || (enc == 2'b01);
assign src1_read = (enc == 2'b00);
assign dstreg_addr = instruction[22:18];
assign src0_addr = instruction[17:13];
assign src1_addr = instruction[12:8];
assign imm_sm = instruction[12:0];
assign imm_lg = instruction[22:0];

// break apart subinstr
assign enc = subinstr[7:6];
assign op = subinstr[5:2];
assign sf = subinstr[1:0];

wire arithmetic;
wire comparison;
wire logical;
wire shift;
assign arithmetic = (op[3:2] == 2'b00);
assign comparison = (op[3:2] == 2'b01) || (op[3:1] == 3'b100);
assign logical    = (op[3:1] == 3'b101);
assign shift      = (op == 4'b1101);
assign src_mask   = (op[3:1] == 3'b111); // TODO: preserve mask state
assign dst_mask   = comparison | (op[3:1] == 3'b111); // TODO: restore mask state

assign immediate =
    enc == 2'b00 ? 0      : // rtype?
    enc == 2'b01 ? imm_sm : // itype?
    enc == 2'b10 ? imm_lg : // ptype?
    0;                      // otype?
assign use_imm = enc[1] ^ enc[0];



endmodule
