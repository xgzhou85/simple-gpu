module vregfile(
    input  clk,
    input  rst_n,
    input  [31:0] dstreg_write,
    input  src0_read,
    input  src1_read,
    input  [4:0]    dstreg_addr,
    input  [4:0]    src0_addr,
    input  [4:0]    src1_addr,
    input  [1023:0] dstreg_data,
    output [1023:0] src0_data,
    output [1023:0] src1_data
);

wire [31:0] dstreg_data_sub[32];
wire [31:0] src0_data_sub[32];
wire [31:0] src1_data_sub[32];

genvar i;
generate
for (i = 0; i < 32; i = i + 1) begin : VREGFILE_REGFILES
    regfile_m10k u_regfile(
        .clk(clk),
        .rst_n(rst_n),
        .dstreg_write(dstreg_write[i]),
        .src0_read(src0_read),
        .src1_read(src1_read),
        .dstreg_addr(dstreg_addr),
        .src0_addr(src0_addr),
        .src1_addr(src1_addr),
        .dstreg_data(dstreg_data_sub[i]),
        .src0_data(src0_data_sub[i]),
        .src1_data(src1_data_sub[i])
    );
end
endgenerate

generate
for (i = 0; i < 32; i = i + 1) begin : VREGFILE_AGGREGATE_REGDATA
    assign src0_data[32*i+:32] = src0_data_sub[i];
    assign src1_data[32*i+:32] = src1_data_sub[i];
    assign dstreg_data_sub[i] = dstreg_data[32*i+:32];
end
endgenerate



endmodule
