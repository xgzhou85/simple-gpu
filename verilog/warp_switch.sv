module warp_switch #(
    parameter MAX_WARPS = 16,
    parameter NUM_CHANNELS = 2,
    parameter MASKS_PER_THREAD = 4
) (
    // control signals from scheduler
    input  [$clog2(MAX_WARPS)-1:0] channel_selector[NUM_CHANNELS],
    input  [$clog2(NUM_CHANNELS)-1:0] context_selector[MAX_WARPS],

    // contexts -> channels
    input  [31:0]   ctxt_program_counter[MAX_WARPS],
    output [31:0]   chnl_program_counter[NUM_CHANNELS],
    input  [31:0]   ctxt_lanes_enabled[MAX_WARPS],
    output [31:0]   chnl_lanes_enabled[NUM_CHANNELS],
    input  [1023:0] ctxt_src0_data[MAX_WARPS],
    output [1023:0] chnl_src0_data[NUM_CHANNELS],
    input  [1023:0] ctxt_src1_data[MAX_WARPS],
    output [1023:0] chnl_src1_data[NUM_CHANNELS],

    // channels -> contexts
    input           chnl_set_program_counter[NUM_CHANNELS],
    output          ctxt_set_program_counter[MAX_WARPS],
    input  [31:0]   chnl_next_program_counter[NUM_CHANNELS],
    output [31:0]   ctxt_next_program_counter[MAX_WARPS],
    input  [31:0]   chnl_dstreg_write[NUM_CHANNELS],
    output [31:0]   ctxt_dstreg_write[MAX_WARPS],
    input           chnl_src0_read[NUM_CHANNELS],
    output          ctxt_src0_read[MAX_WARPS],
    input           chnl_src1_read[NUM_CHANNELS],
    output          ctxt_src1_read[MAX_WARPS],
    input  [4:0]    chnl_dstreg_addr[NUM_CHANNELS],
    output [4:0]    ctxt_dstreg_addr[MAX_WARPS],
    input  [4:0]    chnl_src0_addr[NUM_CHANNELS],
    output [4:0]    ctxt_src0_addr[MAX_WARPS],
    input  [4:0]    chnl_src1_addr[NUM_CHANNELS],
    output [4:0]    ctxt_src1_addr[MAX_WARPS],
    input  [1023:0] chnl_dstreg_data[NUM_CHANNELS],
    output [1023:0] ctxt_dstreg_data[MAX_WARPS],

    input           chnl_mask_write[NUM_CHANNELS],
    output          ctxt_mask_write[MAX_WARPS],
    input           chnl_mask_read[NUM_CHANNELS],
    output          ctxt_mask_read[MAX_WARPS],
    input  [31:0]   chnl_mask_dstreg_data[NUM_CHANNELS],
    output [31:0]   ctxt_mask_dstreg_data[MAX_WARPS]
);

// contexts -> channels
genvar i;
generate
for (i = 0; i < NUM_CHANNELS; i = i + 1) begin : WARP_SWITCH_CHANNEL_SIDE
    assign chnl_program_counter[i] = ctxt_program_counter[channel_selector[i]];
    assign chnl_lanes_enabled[i] = ctxt_lanes_enabled[channel_selector[i]];
    assign chnl_src0_data[i] = ctxt_src0_data[channel_selector[i]];
    assign chnl_src1_data[i] = ctxt_src1_data[channel_selector[i]];
end
endgenerate

// channels -> contexts
generate
for (i = 0; i < MAX_WARPS; i = i + 1) begin : WARP_SWITCH_CONTEXT_SIDE
    assign ctxt_set_program_counter[i]  = chnl_set_program_counter[context_selector[i]];
    assign ctxt_next_program_counter[i] = chnl_next_program_counter[context_selector[i]];
    assign ctxt_dstreg_write[i]         = chnl_dstreg_write[context_selector[i]];
    assign ctxt_src0_read[i]            = chnl_src0_read[context_selector[i]];
    assign ctxt_src1_read[i]            = chnl_src1_read[context_selector[i]];
    assign ctxt_dstreg_addr[i]          = chnl_dstreg_addr[context_selector[i]];
    assign ctxt_src0_addr[i]            = chnl_src0_addr[context_selector[i]];
    assign ctxt_src1_addr[i]            = chnl_src1_addr[context_selector[i]];
    assign ctxt_dstreg_data[i]          = chnl_dstreg_data[context_selector[i]];
    assign ctxt_mask_write[i]           = chnl_mask_write[context_selector[i]];
    assign ctxt_mask_read[i]            = chnl_mask_read[context_selector[i]];
    assign ctxt_mask_dstreg_data[i]     = chnl_mask_dstreg_data[context_selector[i]];
end
endgenerate


endmodule
