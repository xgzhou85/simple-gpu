module warp_context #(
    parameter MASKS_PER_THREAD = 4
)
(
    input clk,
    input rst_n,

    input      set_enabled,
    input      clr_enabled,
    output reg enabled,

    input            set_active,
    input            clr_active,
    output reg       active,

    input             set_program_counter,
    input      [31:0] new_program_counter,
    output reg [31:0] program_counter,

    // mask regs
    input                                 mask_write,
    input                                 mask_read,
    input  [$clog2(MASKS_PER_THREAD)-1:0] mask_wr_addr,
    input  [$clog2(MASKS_PER_THREAD)-1:0] mask_rd0_addr,
    input  [$clog2(MASKS_PER_THREAD)-1:0] mask_rd1_addr,
    input  [31:0]                         mask_wr_data,
    input  [31:0]                         mask_rd0_data,
    input  [31:0]                         mask_rd1_data,
    output [31:0]                         lanes_enabled



);

// instantiate mask regs
genvar i;
generate
for (i = 0; i < 32; i = i + 1) begin : WARP_CONTEXT_MASK_REGS
    mask_reg #(
        .MASKS_PER_THREAD(MASKS_PER_THREAD)
    ) u_mask_reg (
        .clk(clk),
        .rst_n(rst_n),
        .read(mask_read),
        .rd0_addr(mask_rd0_addr),
        .rd1_addr(mask_rd1_addr),
        .rd0_data(mask_rd0_data[i]),
        .rd1_data(mask_rd1_data[i]),
        .write(active && mask_write),
        .wr_addr(mask_wr_addr),
        .wr_data(mask_wr_data[i]),
        .lane_enabled(lanes_enabled[i]),
        .dump()
    );
end
endgenerate


// store everything when appropriate
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) enabled <= 1'b0;
    else if (set_enabled) enabled <= 1'b1;
    else if (clr_enabled) enabled <= 1'b0;
    else enabled <= enabled;

    if (~rst_n) active <= 1'b0;
    else if (set_active) active <= 1'b1;
    else if (clr_active) active <= 1'b0;
    else active <= active;

    if (~rst_n) program_counter <= 0;
    else if (active && set_program_counter) program_counter <= new_program_counter;
    else program_counter <= program_counter;

end



endmodule
