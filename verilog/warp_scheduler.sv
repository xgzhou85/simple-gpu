// TODO: move the instruction buffer out of the warp scheduler
// TODO: probably simpler to just allow the warp_info to store its pc
// the warp scheduler manages the warp information and 

module warp_scheduler #(
    parameter MAX_WARPS = 64,
    parameter NUM_CHANNELS = 4
) (

    input clk,
    input rst_n,

    // command interface
    input        start_kernel,
    input        set_threadcount,
    input [$clog2(MAX_WARPS*32):0] new_threadcount,
    output reg   kernel_active, // block the command interface

    // switch control
    output reg [$clog2(MAX_WARPS)-1:0]    channel_selector[NUM_CHANNELS],
    output reg [$clog2(NUM_CHANNELS)-1:0] context_selector[MAX_WARPS],

    // warp contexts
    output reg [MAX_WARPS-1:0] set_context_enabled,
    output reg [MAX_WARPS-1:0] clr_context_enabled,

    output reg [MAX_WARPS-1:0] set_context_active,
    output reg [MAX_WARPS-1:0] clr_context_active,
    input  reg [MAX_WARPS-1:0] context_active,

    // channels
    output reg [NUM_CHANNELS-1:0] set_channel_active,

    // from writeback probably
    input [NUM_CHANNELS-1:0] retire_channels



);
localparam MAX_THREADS = MAX_WARPS * 32;

// --- GLOBAL STATE --- //

// incrementing this actually increments by 2, since interface is 64b long
// threadcount is set with a set threadcount command before kernel starts
reg [$clog2(MAX_WARPS*32):0] threadcount;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) threadcount <= 0; // inactive
    else if (set_threadcount) threadcount <= new_threadcount;
    else threadcount <= threadcount;
end

// enable warp info when new threadcount is set
genvar i;
generate
for(i = 0; i < MAX_WARPS; i = i + 1) begin : WARP_SCHEDULER_SET_CLR_CONTEXT_ENABLED
    assign set_context_enabled[i] = set_threadcount ?  (i < (threadcount/32)) : 0;
    assign clr_context_enabled[i] = set_threadcount ? ~(i < (threadcount/32)) : 0;
end
endgenerate

// block the command interface if a kernel is running
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) kernel_active <= 0;
    else if (start_kernel) kernel_active <= 1;
    else kernel_active <= kernel_active;
end

// --- WARP SELECTOR --- //
// can issue one warp per cycle for now
// this will probably be a bottleneck
// issues the warps round-robin
reg [NUM_CHANNELS-1:0] channels_available;
reg [NUM_CHANNELS-1:0] next_channels_available;
reg [$clog2(MAX_WARPS)-1:0] channel_assignments[NUM_CHANNELS];
reg [$clog2(MAX_WARPS)-1:0] next_channel_assignments[NUM_CHANNELS];
reg [$clog2(NUM_CHANNELS)-1:0] context_assignments[MAX_WARPS];
reg [$clog2(NUM_CHANNELS)-1:0] next_context_assignments[MAX_WARPS];
reg [$clog2(NUM_CHANNELS)-1:0]  next_channel;
reg [$clog2(MAX_WARPS)-1:0] context_num;

reg issue_warp;
always_ff @(posedge clk, negedge rst_n) begin
    // warp num
    if (~rst_n) context_num <= 0;
    else if (context_num == (threadcount/32)-1) context_num <= 0; // TODO: edge case when threadcount not multiple 32
    else if (issue_warp) context_num <= context_num + 1;
    else context_num <= context_num;

    // channels_available
    if (~rst_n) channels_available <= -1; // set all bits
    else channels_available <= next_channels_available;

    // channel_assignments
    if (~rst_n) for (int i = 0; i < NUM_CHANNELS; i = i + 1) channel_assignments[i] <= 0;
    else if (issue_warp) channel_assignments <= next_channel_assignments;
    else channel_assignments <= channel_assignments;

    // context_assignments
    if (~rst_n) for (int i = 0; i < MAX_WARPS; i = i + 1) context_assignments[i] <= 0;
    else if (issue_warp) context_assignments <= next_context_assignments;
    else context_assignments <= context_assignments;
end

always_comb begin

    // reset all warp control signals to avoid latches
    for (int i = 0; i < MAX_WARPS; i=i+1) begin
        set_context_active[i] = 0;
        clr_context_active[i] = 0;
    end
    for (int i = 0; i < NUM_CHANNELS; i=i+1) begin
        set_channel_active[i] = 0;
    end

    issue_warp = 0;
    next_channel = 0;
    next_channels_available = channels_available;
    next_channel_assignments = channel_assignments;
    next_context_assignments = context_assignments;

    // accept retired warps
    // FIXME: optimize this with a small network or something, this is probably terrible
    next_channels_available = next_channels_available | retire_channels;
    for (int i_ctxt = 0; i_ctxt < MAX_WARPS;    i_ctxt = i_ctxt + 1) begin // warp
    for (int j_chnl = 0; j_chnl < NUM_CHANNELS; j_chnl = j_chnl + 1) begin // active channel
        if(context_assignments[i_ctxt] == j_chnl && context_active[i_ctxt] && retire_channels[j_chnl]) begin
            clr_context_active[i_ctxt] = 1;
            next_context_assignments[i_ctxt] = 0;
            next_channel_assignments[j_chnl] = 0;
        end
    end
    end

    // find the next available channel
    // FIXME: this channel selector is probably not effcient
    if (kernel_active && |next_channels_available && ~context_active[context_num]) begin
        issue_warp = 1;
        for (int i = NUM_CHANNELS-1; i >= 0; i=i-1) begin
            if (next_channels_available[i]) next_channel = i;
        end
    end
    
    // issue a warp to the next available channel
    if (issue_warp) begin
        set_context_active[context_num] = 1;
        set_channel_active[next_channel] = 1;
        next_channel_assignments[next_channel] = context_num;
        next_context_assignments[context_num] = next_channel;
        next_channels_available[next_channel] = 0;
    end
end

generate
for (i = 0; i < NUM_CHANNELS; i = i + 1) begin : WARP_SCHEDULER_CONTEXT_SELECTOR
    assign channel_selector[i] = channel_assignments[i];
end
endgenerate
generate
for (i = 0; i < MAX_WARPS; i = i + 1) begin : WARP_SCHEDULER_CHANNEL_SELECTOR
    assign context_selector[i] = context_assignments[i];
end
endgenerate

endmodule
