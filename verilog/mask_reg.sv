module mask_reg #(
    parameter MASKS_PER_THREAD = 4
)
(
    input       clk,
    input       rst_n,

    input       read,
    input [$clog2(MASKS_PER_THREAD)-1:0] rd0_addr,
    input [$clog2(MASKS_PER_THREAD)-1:0] rd1_addr,
    output      rd0_data,
    output      rd1_data,

    input       write,
    input [$clog2(MASKS_PER_THREAD)-1:0] wr_addr,
    input       wr_data,

    input  unconditional,
    output lane_enabled,
    output [31:0] dump

);

reg [MASKS_PER_THREAD-1:0] masks;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) for (int i = 0; i < MASKS_PER_THREAD; i = i + 1) masks[i] <= 1; // set all active
    else if ((write & lane_enabled) | unconditional) masks[wr_addr] <= wr_data;
    else masks <= masks;
end

assign rd0_data = read ? masks[rd0_addr] : 0;
assign rd1_data = read ? masks[rd1_addr] : 0;
assign lane_enabled = masks[0];
assign dump = masks;

endmodule
