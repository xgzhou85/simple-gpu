// FIXME: this whole module, the avalon memory interface is goofy
module core_mem_arb #(
    parameter NUM_CHANNELS = 4
) (
    input clk,
    input rst_n,

    // from channel ifetch
    input      [31:0] program_counter[NUM_CHANNELS],
    output reg [31:0] instruction[NUM_CHANNELS],
    input      ifetch[NUM_CHANNELS],
    output reg fetched[NUM_CHANNELS],

    // TODO: loadstore
    input                         load[NUM_CHANNELS],
    input                         store[NUM_CHANNELS],
    input      [31:0]             loadstore_address[NUM_CHANNELS],
    output reg [31:0]             loadstore_readdata[NUM_CHANNELS],
    input      [31:0]             loadstore_writedata[NUM_CHANNELS],
    output reg                    loadstore_served[NUM_CHANNELS],

    // ddr3 interface
    output reg [28:0] core_address,
    output reg [7:0]  core_burstcount,
    input             core_waitrequest,
    input  [63:0]     core_readdata,
    input             core_readdatavalid,
    output reg        core_read,
    output reg [63:0] core_writedata,
    output reg [7:0]  core_byteenable,
    output reg        core_write
);

// always prioritize instruction fetch over loadstore

// choose who gets served
// TODO: serve to anyone requesting this (broadcast)

// if it's your turn and you don't fetch, you lose your turn
reg [$clog2(NUM_CHANNELS):0] priority_channel;
reg any_served; // effectively a reduction OR but for a packed type bc verilog
always_comb begin
    any_served = 0;
    for (int i = 0; i < NUM_CHANNELS; i = i + 1) begin
        if (fetched[i] | loadstore_served[i]) any_served <= 1;
    end
end
always_ff @(posedge clk, negedge rst_n) begin

    if (~rst_n) priority_channel <= 0;
    else if (any_served && ((priority_channel+1) != NUM_CHANNELS)) priority_channel <= priority_channel + 1;
    else if (any_served && ((priority_channel+1) == NUM_CHANNELS)) priority_channel <= 0; // not power of 2
    else priority_channel <= priority_channel;
end

typedef enum reg [2:0] {SELECT, IREQUEST, IRECEIVE, LREQUEST, LRECEIVE, SREQUEST} state_t;
state_t state, next_state;
reg serve;
reg next_serve;
reg [$clog2(NUM_CHANNELS):0] serve_channel;
reg [$clog2(NUM_CHANNELS):0] next_serve_channel;

always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) state <= SELECT;
    else state <= next_state;

    if (~rst_n) serve <= 0;
    else serve <= next_serve;

    if (~rst_n) serve_channel <= 0;
    else serve_channel <= next_serve_channel;
end


always_comb begin
    core_address = 0;
    core_burstcount = 1;
    core_read = 0;
    core_writedata = 0;
    core_byteenable = 8'b11111111;
    core_write = 0;

    next_state = SELECT;
    next_serve = 0;
    next_serve_channel = 0;

    for (int i = 0; i < NUM_CHANNELS; i = i + 1) begin
        instruction[i] = 32'h00000000; // TODO: NOP
        loadstore_readdata[i] = 0;
        loadstore_served[i] = 0;
        fetched[i] = 0;
    end

    case(state)
    SELECT: begin
        for (int i_chnl = 0; i_chnl < NUM_CHANNELS; i_chnl = i_chnl + 1) begin
            if (ifetch[i_chnl] | load[i_chnl] | store[i_chnl]) begin
                next_serve = 1;
                next_serve_channel = i_chnl;
                if      (ifetch[i_chnl]) next_state = IREQUEST;
                else if (load[i_chnl])   next_state = LREQUEST;
                else next_state = SREQUEST;
                if (i_chnl == priority_channel) break; // quit if prioritized one has it
            end
        end
    end

    IREQUEST: begin
        if (~core_waitrequest) begin
            core_address = program_counter[serve_channel] >> 6;
            core_read = 1;
            next_state = IRECEIVE;
            next_serve = 1;
            next_serve_channel = serve_channel;
        end else begin
            next_state = IREQUEST; // stay
            next_serve = 1;
            next_serve_channel = serve_channel;
        end
    end

    IRECEIVE: begin
        if (core_readdatavalid) begin
            fetched[serve_channel] = 1;
            instruction[serve_channel] = program_counter[serve_channel][2] ? core_readdata[63:32] : core_readdata[31:0];
            next_state = SELECT;
        end else begin
            next_state = IRECEIVE; // stay
            next_serve = 1;
            next_serve_channel = serve_channel;
        end
    end

    LREQUEST: begin
        if (~core_waitrequest) begin
            core_address = loadstore_address[serve_channel] >> 6;
            core_read = 1;
            next_state = LRECEIVE;
            next_serve = 1;
            next_serve_channel = serve_channel;
        end else begin
            next_state = LREQUEST; // stay
            next_serve = 1;
            next_serve_channel = serve_channel;
        end
    end

    LRECEIVE: begin
        if (core_readdatavalid) begin
            loadstore_served[serve_channel] = 1;
            loadstore_readdata[serve_channel] = loadstore_address[serve_channel][2] ? core_readdata[63:32] : core_readdata[31:0];
            next_state = SELECT;
        end else begin
            next_state = LRECEIVE; // stay
            next_serve = 1;
            next_serve_channel = serve_channel;
        end
    end

    // without a cache, writes are super broken because the interface size > word size and things will get zeroed out
    // TODO: add a cache, it's pretty mandatory because of the avalon interface
    SREQUEST: begin
        if (~core_waitrequest) begin
            core_address = loadstore_address[serve_channel] >> 6;
            core_writedata = loadstore_writedata[serve_channel];
            core_write = 1;
            next_state = SELECT;
        end else begin
            next_state = SREQUEST; // stay
        end
    end
    
    endcase
end
endmodule
