
//=============================================================================
// This module manages reading from and writing to a frame buffer located in a 
// 1GB ddr3 memory interfaced by an avalon-mm interface
//=============================================================================


localparam TRANSACTION_COUNT = 153600;// PIXELS_HORIZ*PIXELS_VERT*32/64;
module framebuffer_intf_cached #(
    parameter PIXELS_HORIZ = 640,
    parameter PIXELS_VERT  = 480
) (
    input clk,
    input rst_n,

    input  wire [$clog2(PIXELS_HORIZ):0] xpos,
    input  wire [$clog2(PIXELS_VERT ):0] ypos,
    output reg  [23:0] outdata,
    input  wire read,

    output wire [28:0] framebuffer_address,
    output wire [7:0]  framebuffer_burstcount,
    input  wire        framebuffer_waitrequest,
    input  wire [63:0] framebuffer_readdata,
    input  wire        framebuffer_readdatavalid,
    output wire        framebuffer_read,
    output wire [7:0]  framebuffer_byteenable,

    input  wire [31:0] framebuffer_location,
    input  wire        framebuffer_enabled


);

// transaction counter
reg [$clog2(TRANSACTION_COUNT):0] transaction_count;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) transaction_count <= 0; 
    else if (transaction_count == TRANSACTION_COUNT) transaction_count <= 0;
    else if (read) transaction_count <= transaction_count + 1;
    else transaction_count <= transaction_count;
end


// odd/even alternating bit
reg oddeven;
wire shift_buffer;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) oddeven <= 0;
    else if (read) oddeven <= ~oddeven;
    else oddeven <= oddeven;
end
assign shift_buffer = oddeven & read;

// sr latch
reg service;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) service <= 0;
    else if (shift_buffer) service <= 1;
    else if (framebuffer_read) service <= 0;
    else service <= service;
end


// pixel buffer holds 4 pixels
reg [23:0] pixel_buffer[4];
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) begin 
        for (int i = 0; i < 4; i = i + 1) pixel_buffer[i] <= 24'hffffff;
    end
    else begin 
        if (shift_buffer) begin
            pixel_buffer[0] <= pixel_buffer[2];
            pixel_buffer[1] <= pixel_buffer[3];
        end
        if (framebuffer_readdatavalid) begin
            pixel_buffer[2] <= framebuffer_readdata[23:0];
            pixel_buffer[3] <= framebuffer_readdata[55:32];
        end
    end
end
assign outdata = pixel_buffer[oddeven];

assign framebuffer_address = (framebuffer_location >> 6) + transaction_count;
assign framebuffer_read = framebuffer_enabled & service & ~framebuffer_waitrequest;
assign framebuffer_burstcount = 1;
assign framebuffer_byteenable = 8'b11111111;




endmodule
