module tb_warp_switch();

localparam MAX_WARPS = 4;
localparam NUM_CHANNELS = 2;

logic clk;

// control signals from scheduler
logic  [$clog2(MAX_WARPS)-1:0] channel_selector[NUM_CHANNELS];
logic  [$clog2(NUM_CHANNELS)-1:0] context_selector[MAX_WARPS];

// contexts -> channels
logic  [31:0]   ctxt_program_counter[MAX_WARPS];
logic  [31:0]   chnl_program_counter[NUM_CHANNELS];
logic  [31:0]   ctxt_lanes_enabled[MAX_WARPS];
logic  [31:0]   chnl_lanes_enabled[NUM_CHANNELS];
logic  [1023:0] ctxt_src0_data[MAX_WARPS];
logic  [1023:0] chnl_src0_data[NUM_CHANNELS];
logic  [1023:0] ctxt_src1_data[MAX_WARPS];
logic  [1023:0] chnl_src1_data[NUM_CHANNELS];

// channels -> contexts
logic           chnl_set_program_counter[NUM_CHANNELS];
logic           ctxt_set_program_counter[MAX_WARPS];
logic  [31:0]   chnl_next_program_counter[NUM_CHANNELS];
logic  [31:0]   ctxt_next_program_counter[MAX_WARPS];
logic  [31:0]   chnl_dstreg_write[NUM_CHANNELS];
logic  [31:0]   ctxt_dstreg_write[MAX_WARPS];
logic           chnl_src0_read[NUM_CHANNELS];
logic           ctxt_src0_read[MAX_WARPS];
logic           chnl_src1_read[NUM_CHANNELS];
logic           ctxt_src1_read[MAX_WARPS];
logic  [4:0]    chnl_dstreg_addr[NUM_CHANNELS];
logic  [4:0]    ctxt_dstreg_addr[MAX_WARPS];
logic  [4:0]    chnl_src0_addr[NUM_CHANNELS];
logic  [4:0]    ctxt_src0_addr[MAX_WARPS];
logic  [4:0]    chnl_src1_addr[NUM_CHANNELS];
logic  [4:0]    ctxt_src1_addr[MAX_WARPS];
logic  [1023:0] chnl_dstreg_data[NUM_CHANNELS];
logic  [1023:0] ctxt_dstreg_data[MAX_WARPS];

logic           chnl_mask_write[NUM_CHANNELS];
logic           ctxt_mask_write[MAX_WARPS];
logic           chnl_mask_read[NUM_CHANNELS];
logic           ctxt_mask_read[MAX_WARPS];
logic  [31:0]   chnl_mask_dstreg_data[NUM_CHANNELS];
logic  [31:0]   ctxt_mask_dstreg_data[MAX_WARPS];

warp_switch #(
    .MAX_WARPS(MAX_WARPS),
    .NUM_CHANNELS(NUM_CHANNELS)
) dut (
    .channel_selector          (channel_selector),
    .context_selector          (context_selector),

    .ctxt_program_counter      (ctxt_program_counter),
    .chnl_program_counter      (chnl_program_counter),
    .ctxt_lanes_enabled        (ctxt_lanes_enabled),
    .chnl_lanes_enabled        (chnl_lanes_enabled),
    .ctxt_src0_data            (ctxt_src0_data),
    .chnl_src0_data            (chnl_src0_data),
    .ctxt_src1_data            (ctxt_src1_data),
    .chnl_src1_data            (chnl_src1_data),

    .chnl_set_program_counter  (chnl_set_program_counter),
    .ctxt_set_program_counter  (ctxt_set_program_counter),
    .chnl_next_program_counter (chnl_next_program_counter),
    .ctxt_next_program_counter (ctxt_next_program_counter),
    .chnl_dstreg_write         (chnl_dstreg_write),
    .ctxt_dstreg_write         (ctxt_dstreg_write),
    .chnl_src0_read            (chnl_src0_read),
    .ctxt_src0_read            (ctxt_src0_read),
    .chnl_src1_read            (chnl_src1_read),
    .ctxt_src1_read            (ctxt_src1_read),
    .chnl_dstreg_addr          (chnl_dstreg_addr),
    .ctxt_dstreg_addr          (ctxt_dstreg_addr),
    .chnl_src0_addr            (chnl_src0_addr),
    .ctxt_src0_addr            (ctxt_src0_addr),
    .chnl_src1_addr            (chnl_src1_addr),
    .ctxt_src1_addr            (ctxt_src1_addr),
    .chnl_dstreg_data          (chnl_dstreg_data),
    .ctxt_dstreg_data          (ctxt_dstreg_data),

    .chnl_mask_write           (chnl_mask_write),
    .ctxt_mask_write           (ctxt_mask_write),
    .chnl_mask_read            (chnl_mask_read),
    .ctxt_mask_read            (ctxt_mask_read),
    .chnl_mask_dstreg_data     (chnl_mask_dstreg_data),
    .ctxt_mask_dstreg_data     (ctxt_mask_dstreg_data)
);


always #1 clk <= ~clk;

initial begin
    clk = 0;

    // contexts -> channels
    for (int i = 0; i < MAX_WARPS; i = i + 1) begin
        ctxt_program_counter[i] = i;
        ctxt_lanes_enabled[i]   = i;
        ctxt_src0_data[i]       = {32{i}}; // all regs for context 2 are 2, for example
        ctxt_src1_data[i]       = {32{i}};
    end

    // channels -> contexts
    for (int i = 0; i < NUM_CHANNELS; i = i + 1) begin
        chnl_set_program_counter[i]  = 1;
        chnl_next_program_counter[i] = 32'hbabebabe;
        chnl_dstreg_write[i]         = i;
        chnl_src0_read[i]            = (i%2 == 0);
        chnl_src1_read[i]            = (i%2 != 0);
        chnl_dstreg_addr[i]          = i;
        chnl_src0_addr[i]            = i;
        chnl_src1_addr[i]            = i;
        chnl_dstreg_data[i]          = {32{i}};
        chnl_mask_write[i]           = (i%2 == 0);
        chnl_mask_read[i]            = (i%2 == 0);
        chnl_mask_dstreg_data[i]     = i;
    end

    // set context 2 to channel 1
    channel_selector[0] = 0;
    channel_selector[1] = 2;

    context_selector[0] = 0;
    context_selector[1] = 0;
    context_selector[2] = 1;
    context_selector[3] = 0;

    // check all the signals between context 2 and channel 1
    // contexts -> channels
    #1 // combinational delay
    if (chnl_program_counter[1] !== 2          ) $display("Test failed at line %d", `__LINE__);
    if (chnl_lanes_enabled[1]   !== 2          ) $display("Test failed at line %d", `__LINE__);
    if (chnl_src0_data[1]       !== {32{32'h2}}) $display("Test failed at line %d", `__LINE__);
    if (chnl_src1_data[1]       !== {32{32'h2}}) $display("Test failed at line %d", `__LINE__);

    // channels -> contexts
    if (ctxt_set_program_counter[2]  !== 1           ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_next_program_counter[2] !== 32'hbabebabe) $display("Test failed at line %d", `__LINE__);
    if (ctxt_dstreg_write[2]         !== 1           ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_src0_read[2]            !== (1%2 == 0)  ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_src1_read[2]            !== (1%2 != 0)  ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_dstreg_addr[2]          !== 1           ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_src0_addr[2]            !== 1           ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_src1_addr[2]            !== 1           ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_dstreg_data[2]          !== {32{32'h1}} ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_mask_write[2]           !== (1%2 == 0)  ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_mask_read[2]            !== (1%2 == 0)  ) $display("Test failed at line %d", `__LINE__);
    if (ctxt_mask_dstreg_data[2]     !== 1           ) $display("Test failed at line %d", `__LINE__);

    $display("Test completed.");
    $stop;





end


endmodule
