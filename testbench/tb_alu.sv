// TODO: more extensive testbench, seems okay for now
module tb_alu();

logic [31:0] operand0;
logic [31:0] operand1;
logic [31:0] result;
logic [3:0]  op;
logic [1:0]  sf;

alu #(
    .FXP_INT_BITS(16)
) dut (
    .operand0(operand0),
    .operand1(operand1),
    .result(result),
    .op(op),
    .sf(sf)
);

initial begin
    operand0 = 32'h0;
    operand1 = 32'h0;
    {op, sf} = 0;
    #1;

    // unsigned add
    operand0 = 32'h2;
    operand1 = 32'h2;
    {op, sf} = 6'b000000;
    #1;
    if (result != 4) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // signed sub
    operand0 = 32'h00000002; // 2
    operand1 = 32'hfffffffe; // -2
    {op, sf} = 6'b000101;
    #1;
    if (result != 4) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // signed mul
    operand0 = 32'hfffffffe; // -2
    operand1 = 32'hfffffffe; // -2
    {op, sf} = 6'b001001;
    #1;
    if (result != 4) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // signed + fxp
    operand0 = 32'h00000002;  // 2
    operand1 = 32'h0000_f000; // 0.5
    {op, sf} = 6'b000010;
    #1;
    if (result != 32'h0002_f000) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // signed + fxp
    operand0 = 32'hfffffffe;  // -2
    operand1 = 32'h0000_f000; // 0.5
    {op, sf} = 6'b000010;
    #1;
    if (result != 32'hfffe_f000) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // fxp + fxp
    operand0 = 32'hfffe_0000;  // -2
    operand1 = 32'h0000_f000; // 0.5
    {op, sf} = 6'b000011;
    #1;
    if (result != 32'hfffe_f000) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // unsigned gt
    operand0 = 32'h00000003;
    operand1 = 32'h00000001;
    {op, sf} = 6'b011000;
    #1;
    if (result != 32'h00000001) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // signed gt
    operand0 = 32'h00000001; // 1
    operand1 = 32'hffffffff; // -1
    {op, sf} = 6'b011001;
    #1;
    if (result != 32'h00000001) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // signed/fxp gt
    operand0 = 32'h00000001;  // 1
    operand1 = 32'hfffe_f000; // -1.5
    {op, sf} = 6'b011010;
    #1;
    if (result != 32'h00000001) $display("Test failed at line %d; result = %x", `__LINE__, result);

    // fxp/fxp gt
    operand0 = 32'h0001_0000;  // 1
    operand1 = 32'hfffe_f000; // -1.5
    {op, sf} = 6'b011010;
    #1;
    if (result != 32'h00000001) $display("Test failed at line %d; result = %x", `__LINE__, result);

    $display("Test complete.");
    $stop;

    // logical left shift
    operand0 = 32'h00000001;  // 1
    operand1 = 32'h00000001;  // 1
    {op, sf} = 6'b011010;
    #1;
    if (result != 32'h00000002) $display("Test failed at line %d; result = %x", `__LINE__, result);

    $display("Test complete.");
    $stop;
    
    // arithmetic shift
    operand0 = 32'hfffffffe;  // -2
    operand1 = 32'h00000001;  // 1
    {op, sf} = 6'b011010;
    #1;
    if (result != 32'hffffffff) $display("Test failed at line %d; result = %x", `__LINE__, result);



end



endmodule
