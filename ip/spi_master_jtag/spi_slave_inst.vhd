	component spi_slave is
		port (
			spi_slave_clk_clk         : in    std_logic                    := 'X';             -- clk
			spi_slave_external_mosi   : in    std_logic                    := 'X';             -- mosi
			spi_slave_external_nss    : in    std_logic                    := 'X';             -- nss
			spi_slave_external_miso   : inout std_logic                    := 'X';             -- miso
			spi_slave_external_sclk   : in    std_logic                    := 'X';             -- sclk
			spi_slave_rstn_reset_n    : in    std_logic                    := 'X';             -- reset_n
			spi_slave_st_sink_valid   : in    std_logic                    := 'X';             -- valid
			spi_slave_st_sink_data    : in    std_logic_vector(7 downto 0) := (others => 'X'); -- data
			spi_slave_st_sink_ready   : out   std_logic;                                       -- ready
			spi_slave_st_source_ready : in    std_logic                    := 'X';             -- ready
			spi_slave_st_source_valid : out   std_logic;                                       -- valid
			spi_slave_st_source_data  : out   std_logic_vector(7 downto 0)                     -- data
		);
	end component spi_slave;

	u0 : component spi_slave
		port map (
			spi_slave_clk_clk         => CONNECTED_TO_spi_slave_clk_clk,         --       spi_slave_clk.clk
			spi_slave_external_mosi   => CONNECTED_TO_spi_slave_external_mosi,   --  spi_slave_external.mosi
			spi_slave_external_nss    => CONNECTED_TO_spi_slave_external_nss,    --                    .nss
			spi_slave_external_miso   => CONNECTED_TO_spi_slave_external_miso,   --                    .miso
			spi_slave_external_sclk   => CONNECTED_TO_spi_slave_external_sclk,   --                    .sclk
			spi_slave_rstn_reset_n    => CONNECTED_TO_spi_slave_rstn_reset_n,    --      spi_slave_rstn.reset_n
			spi_slave_st_sink_valid   => CONNECTED_TO_spi_slave_st_sink_valid,   --   spi_slave_st_sink.valid
			spi_slave_st_sink_data    => CONNECTED_TO_spi_slave_st_sink_data,    --                    .data
			spi_slave_st_sink_ready   => CONNECTED_TO_spi_slave_st_sink_ready,   --                    .ready
			spi_slave_st_source_ready => CONNECTED_TO_spi_slave_st_source_ready, -- spi_slave_st_source.ready
			spi_slave_st_source_valid => CONNECTED_TO_spi_slave_st_source_valid, --                    .valid
			spi_slave_st_source_data  => CONNECTED_TO_spi_slave_st_source_data   --                    .data
		);

