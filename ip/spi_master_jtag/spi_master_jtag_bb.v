
module spi_master_jtag (
	clk50_clk,
	rstn_reset_n,
	spi_master_external_MISO,
	spi_master_external_MOSI,
	spi_master_external_SCLK,
	spi_master_external_SS_n,
	spi_master_irq_irq);	

	input		clk50_clk;
	input		rstn_reset_n;
	input		spi_master_external_MISO;
	output		spi_master_external_MOSI;
	output		spi_master_external_SCLK;
	output		spi_master_external_SS_n;
	output		spi_master_irq_irq;
endmodule
