	component spi_master_jtag is
		port (
			clk50_clk                : in  std_logic := 'X'; -- clk
			rstn_reset_n             : in  std_logic := 'X'; -- reset_n
			spi_master_external_MISO : in  std_logic := 'X'; -- MISO
			spi_master_external_MOSI : out std_logic;        -- MOSI
			spi_master_external_SCLK : out std_logic;        -- SCLK
			spi_master_external_SS_n : out std_logic;        -- SS_n
			spi_master_irq_irq       : out std_logic         -- irq
		);
	end component spi_master_jtag;

	u0 : component spi_master_jtag
		port map (
			clk50_clk                => CONNECTED_TO_clk50_clk,                --               clk50.clk
			rstn_reset_n             => CONNECTED_TO_rstn_reset_n,             --                rstn.reset_n
			spi_master_external_MISO => CONNECTED_TO_spi_master_external_MISO, -- spi_master_external.MISO
			spi_master_external_MOSI => CONNECTED_TO_spi_master_external_MOSI, --                    .MOSI
			spi_master_external_SCLK => CONNECTED_TO_spi_master_external_SCLK, --                    .SCLK
			spi_master_external_SS_n => CONNECTED_TO_spi_master_external_SS_n, --                    .SS_n
			spi_master_irq_irq       => CONNECTED_TO_spi_master_irq_irq        --      spi_master_irq.irq
		);

