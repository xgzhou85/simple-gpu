	spi_slave u0 (
		.spi_slave_clk_clk         (<connected-to-spi_slave_clk_clk>),         //       spi_slave_clk.clk
		.spi_slave_external_mosi   (<connected-to-spi_slave_external_mosi>),   //  spi_slave_external.mosi
		.spi_slave_external_nss    (<connected-to-spi_slave_external_nss>),    //                    .nss
		.spi_slave_external_miso   (<connected-to-spi_slave_external_miso>),   //                    .miso
		.spi_slave_external_sclk   (<connected-to-spi_slave_external_sclk>),   //                    .sclk
		.spi_slave_rstn_reset_n    (<connected-to-spi_slave_rstn_reset_n>),    //      spi_slave_rstn.reset_n
		.spi_slave_st_sink_valid   (<connected-to-spi_slave_st_sink_valid>),   //   spi_slave_st_sink.valid
		.spi_slave_st_sink_data    (<connected-to-spi_slave_st_sink_data>),    //                    .data
		.spi_slave_st_sink_ready   (<connected-to-spi_slave_st_sink_ready>),   //                    .ready
		.spi_slave_st_source_ready (<connected-to-spi_slave_st_source_ready>), // spi_slave_st_source.ready
		.spi_slave_st_source_valid (<connected-to-spi_slave_st_source_valid>), //                    .valid
		.spi_slave_st_source_data  (<connected-to-spi_slave_st_source_data>)   //                    .data
	);

