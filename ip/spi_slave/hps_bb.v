
module hps (
	hps_f2h_mpu_events_eventi,
	hps_f2h_mpu_events_evento,
	hps_f2h_mpu_events_standbywfe,
	hps_f2h_mpu_events_standbywfi,
	hps_f2h_sdram0_data_address,
	hps_f2h_sdram0_data_burstcount,
	hps_f2h_sdram0_data_waitrequest,
	hps_f2h_sdram0_data_readdata,
	hps_f2h_sdram0_data_readdatavalid,
	hps_f2h_sdram0_data_read,
	hps_f2h_sdram0_data_writedata,
	hps_f2h_sdram0_data_byteenable,
	hps_f2h_sdram0_data_write,
	hps_f2h_sdram1_data_address,
	hps_f2h_sdram1_data_burstcount,
	hps_f2h_sdram1_data_waitrequest,
	hps_f2h_sdram1_data_readdata,
	hps_f2h_sdram1_data_readdatavalid,
	hps_f2h_sdram1_data_read,
	hps_f2h_sdram1_data_writedata,
	hps_f2h_sdram1_data_byteenable,
	hps_f2h_sdram1_data_write,
	hps_io_hps_io_sdio_inst_CMD,
	hps_io_hps_io_sdio_inst_D0,
	hps_io_hps_io_sdio_inst_D1,
	hps_io_hps_io_sdio_inst_CLK,
	hps_io_hps_io_sdio_inst_D2,
	hps_io_hps_io_sdio_inst_D3,
	hps_io_hps_io_uart0_inst_RX,
	hps_io_hps_io_uart0_inst_TX,
	hps_rstn_bridge_out_reset_n_reset_n,
	hps_sysclk_bridge_in_clk_clk,
	memory_mem_a,
	memory_mem_ba,
	memory_mem_ck,
	memory_mem_ck_n,
	memory_mem_cke,
	memory_mem_cs_n,
	memory_mem_ras_n,
	memory_mem_cas_n,
	memory_mem_we_n,
	memory_mem_reset_n,
	memory_mem_dq,
	memory_mem_dqs,
	memory_mem_dqs_n,
	memory_mem_odt,
	memory_mem_dm,
	memory_oct_rzqin,
	hps_f2h_sdram2_data_address,
	hps_f2h_sdram2_data_burstcount,
	hps_f2h_sdram2_data_waitrequest,
	hps_f2h_sdram2_data_readdata,
	hps_f2h_sdram2_data_readdatavalid,
	hps_f2h_sdram2_data_read,
	hps_f2h_sdram2_data_writedata,
	hps_f2h_sdram2_data_byteenable,
	hps_f2h_sdram2_data_write);	

	input		hps_f2h_mpu_events_eventi;
	output		hps_f2h_mpu_events_evento;
	output	[1:0]	hps_f2h_mpu_events_standbywfe;
	output	[1:0]	hps_f2h_mpu_events_standbywfi;
	input	[28:0]	hps_f2h_sdram0_data_address;
	input	[7:0]	hps_f2h_sdram0_data_burstcount;
	output		hps_f2h_sdram0_data_waitrequest;
	output	[63:0]	hps_f2h_sdram0_data_readdata;
	output		hps_f2h_sdram0_data_readdatavalid;
	input		hps_f2h_sdram0_data_read;
	input	[63:0]	hps_f2h_sdram0_data_writedata;
	input	[7:0]	hps_f2h_sdram0_data_byteenable;
	input		hps_f2h_sdram0_data_write;
	input	[28:0]	hps_f2h_sdram1_data_address;
	input	[7:0]	hps_f2h_sdram1_data_burstcount;
	output		hps_f2h_sdram1_data_waitrequest;
	output	[63:0]	hps_f2h_sdram1_data_readdata;
	output		hps_f2h_sdram1_data_readdatavalid;
	input		hps_f2h_sdram1_data_read;
	input	[63:0]	hps_f2h_sdram1_data_writedata;
	input	[7:0]	hps_f2h_sdram1_data_byteenable;
	input		hps_f2h_sdram1_data_write;
	inout		hps_io_hps_io_sdio_inst_CMD;
	inout		hps_io_hps_io_sdio_inst_D0;
	inout		hps_io_hps_io_sdio_inst_D1;
	output		hps_io_hps_io_sdio_inst_CLK;
	inout		hps_io_hps_io_sdio_inst_D2;
	inout		hps_io_hps_io_sdio_inst_D3;
	input		hps_io_hps_io_uart0_inst_RX;
	output		hps_io_hps_io_uart0_inst_TX;
	output		hps_rstn_bridge_out_reset_n_reset_n;
	input		hps_sysclk_bridge_in_clk_clk;
	output	[14:0]	memory_mem_a;
	output	[2:0]	memory_mem_ba;
	output		memory_mem_ck;
	output		memory_mem_ck_n;
	output		memory_mem_cke;
	output		memory_mem_cs_n;
	output		memory_mem_ras_n;
	output		memory_mem_cas_n;
	output		memory_mem_we_n;
	output		memory_mem_reset_n;
	inout	[31:0]	memory_mem_dq;
	inout	[3:0]	memory_mem_dqs;
	inout	[3:0]	memory_mem_dqs_n;
	output		memory_mem_odt;
	output	[3:0]	memory_mem_dm;
	input		memory_oct_rzqin;
	input	[28:0]	hps_f2h_sdram2_data_address;
	input	[7:0]	hps_f2h_sdram2_data_burstcount;
	output		hps_f2h_sdram2_data_waitrequest;
	output	[63:0]	hps_f2h_sdram2_data_readdata;
	output		hps_f2h_sdram2_data_readdatavalid;
	input		hps_f2h_sdram2_data_read;
	input	[63:0]	hps_f2h_sdram2_data_writedata;
	input	[7:0]	hps_f2h_sdram2_data_byteenable;
	input		hps_f2h_sdram2_data_write;
endmodule
