	vga_ctl u0 (
		.clock_bridge_50_in_clk_clk                           (<connected-to-clock_bridge_50_in_clk_clk>),                           //                    clock_bridge_50_in_clk.clk
		.frame_pll_outclk_clk                                 (<connected-to-frame_pll_outclk_clk>),                                 //                          frame_pll_outclk.clk
		.reset_bridge_0_in_reset_reset_n                      (<connected-to-reset_bridge_0_in_reset_reset_n>),                      //                   reset_bridge_0_in_reset.reset_n
		.video_vga_controller_0_avalon_vga_sink_data          (<connected-to-video_vga_controller_0_avalon_vga_sink_data>),          //    video_vga_controller_0_avalon_vga_sink.data
		.video_vga_controller_0_avalon_vga_sink_startofpacket (<connected-to-video_vga_controller_0_avalon_vga_sink_startofpacket>), //                                          .startofpacket
		.video_vga_controller_0_avalon_vga_sink_endofpacket   (<connected-to-video_vga_controller_0_avalon_vga_sink_endofpacket>),   //                                          .endofpacket
		.video_vga_controller_0_avalon_vga_sink_valid         (<connected-to-video_vga_controller_0_avalon_vga_sink_valid>),         //                                          .valid
		.video_vga_controller_0_avalon_vga_sink_ready         (<connected-to-video_vga_controller_0_avalon_vga_sink_ready>),         //                                          .ready
		.video_vga_controller_0_external_interface_CLK        (<connected-to-video_vga_controller_0_external_interface_CLK>),        // video_vga_controller_0_external_interface.CLK
		.video_vga_controller_0_external_interface_HS         (<connected-to-video_vga_controller_0_external_interface_HS>),         //                                          .HS
		.video_vga_controller_0_external_interface_VS         (<connected-to-video_vga_controller_0_external_interface_VS>),         //                                          .VS
		.video_vga_controller_0_external_interface_BLANK      (<connected-to-video_vga_controller_0_external_interface_BLANK>),      //                                          .BLANK
		.video_vga_controller_0_external_interface_SYNC       (<connected-to-video_vga_controller_0_external_interface_SYNC>),       //                                          .SYNC
		.video_vga_controller_0_external_interface_R          (<connected-to-video_vga_controller_0_external_interface_R>),          //                                          .R
		.video_vga_controller_0_external_interface_G          (<connected-to-video_vga_controller_0_external_interface_G>),          //                                          .G
		.video_vga_controller_0_external_interface_B          (<connected-to-video_vga_controller_0_external_interface_B>)           //                                          .B
	);

