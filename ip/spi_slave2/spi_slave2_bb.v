
module spi_slave2 (
	spi_slave_clk_clk,
	spi_slave_rstn_reset_n,
	spi_slave_ctl_writedata,
	spi_slave_ctl_readdata,
	spi_slave_ctl_address,
	spi_slave_ctl_read_n,
	spi_slave_ctl_chipselect,
	spi_slave_ctl_write_n,
	spi_slave_irq_irq,
	spi_slave_external_MISO,
	spi_slave_external_MOSI,
	spi_slave_external_SCLK,
	spi_slave_external_SS_n);	

	input		spi_slave_clk_clk;
	input		spi_slave_rstn_reset_n;
	input	[15:0]	spi_slave_ctl_writedata;
	output	[15:0]	spi_slave_ctl_readdata;
	input	[2:0]	spi_slave_ctl_address;
	input		spi_slave_ctl_read_n;
	input		spi_slave_ctl_chipselect;
	input		spi_slave_ctl_write_n;
	output		spi_slave_irq_irq;
	output		spi_slave_external_MISO;
	input		spi_slave_external_MOSI;
	input		spi_slave_external_SCLK;
	input		spi_slave_external_SS_n;
endmodule
