	component spi_slave2 is
		port (
			spi_slave_clk_clk        : in  std_logic                     := 'X';             -- clk
			spi_slave_rstn_reset_n   : in  std_logic                     := 'X';             -- reset_n
			spi_slave_ctl_writedata  : in  std_logic_vector(15 downto 0) := (others => 'X'); -- writedata
			spi_slave_ctl_readdata   : out std_logic_vector(15 downto 0);                    -- readdata
			spi_slave_ctl_address    : in  std_logic_vector(2 downto 0)  := (others => 'X'); -- address
			spi_slave_ctl_read_n     : in  std_logic                     := 'X';             -- read_n
			spi_slave_ctl_chipselect : in  std_logic                     := 'X';             -- chipselect
			spi_slave_ctl_write_n    : in  std_logic                     := 'X';             -- write_n
			spi_slave_irq_irq        : out std_logic;                                        -- irq
			spi_slave_external_MISO  : out std_logic;                                        -- MISO
			spi_slave_external_MOSI  : in  std_logic                     := 'X';             -- MOSI
			spi_slave_external_SCLK  : in  std_logic                     := 'X';             -- SCLK
			spi_slave_external_SS_n  : in  std_logic                     := 'X'              -- SS_n
		);
	end component spi_slave2;

	u0 : component spi_slave2
		port map (
			spi_slave_clk_clk        => CONNECTED_TO_spi_slave_clk_clk,        --      spi_slave_clk.clk
			spi_slave_rstn_reset_n   => CONNECTED_TO_spi_slave_rstn_reset_n,   --     spi_slave_rstn.reset_n
			spi_slave_ctl_writedata  => CONNECTED_TO_spi_slave_ctl_writedata,  --      spi_slave_ctl.writedata
			spi_slave_ctl_readdata   => CONNECTED_TO_spi_slave_ctl_readdata,   --                   .readdata
			spi_slave_ctl_address    => CONNECTED_TO_spi_slave_ctl_address,    --                   .address
			spi_slave_ctl_read_n     => CONNECTED_TO_spi_slave_ctl_read_n,     --                   .read_n
			spi_slave_ctl_chipselect => CONNECTED_TO_spi_slave_ctl_chipselect, --                   .chipselect
			spi_slave_ctl_write_n    => CONNECTED_TO_spi_slave_ctl_write_n,    --                   .write_n
			spi_slave_irq_irq        => CONNECTED_TO_spi_slave_irq_irq,        --      spi_slave_irq.irq
			spi_slave_external_MISO  => CONNECTED_TO_spi_slave_external_MISO,  -- spi_slave_external.MISO
			spi_slave_external_MOSI  => CONNECTED_TO_spi_slave_external_MOSI,  --                   .MOSI
			spi_slave_external_SCLK  => CONNECTED_TO_spi_slave_external_SCLK,  --                   .SCLK
			spi_slave_external_SS_n  => CONNECTED_TO_spi_slave_external_SS_n   --                   .SS_n
		);

